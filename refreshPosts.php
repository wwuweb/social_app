<?php
/**
 * Created by PhpStorm.
 * User: waltria
 * Date: 5/30/17
 * Time: 3:52 PM
 *
 * Description:  Run this script to update last_update_date of posts.
 *     Should run from cron job like once a day.
 */
function email_error_handler($number, $message, $file, $line, $vars){
    $msg = "
        <p><strong>refreshPosts.php:  An error occurred while running cron job refreshPosts:
        <strong>Error $number, $message at line $line.</strong>";
    $headers = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    // Email the error to someone...
    error_log($msg, 1, 'web.help@wwu.edu', $headers);
}

set_error_handler('email_error_handler');

$ch = curl_init();
// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, "https://social.wwu.edu/index.php/statistics/runSloggers/facebook/0/5");
curl_setopt($ch, CURLOPT_HEADER, 0);
// grab URL and pass it to the browser
curl_exec($ch);

curl_setopt($ch, CURLOPT_URL, "https://social.wwu.edu/index.php/statistics/runSloggers/twitter/0/5");
curl_setopt($ch, CURLOPT_HEADER, 0);
// grab URL and pass it to the browser
curl_exec($ch);

curl_setopt($ch, CURLOPT_URL, "https://social.wwu.edu/index.php/statistics/runSloggers/instagram/0/5");
curl_setopt($ch, CURLOPT_HEADER, 0);
// grab URL and pass it to the browser
curl_exec($ch);

curl_setopt($ch, CURLOPT_URL, "https://social.wwu.edu/index.php/statistics/runSloggers/youtube/0/5");
curl_setopt($ch, CURLOPT_HEADER, 0);
// grab URL and pass it to the browser
curl_exec($ch);

curl_setopt($ch, CURLOPT_URL, "https://social.wwu.edu/index.php/statistics/runSloggers/flickr/0/5");
curl_setopt($ch, CURLOPT_HEADER, 0);
// grab URL and pass it to the browser
curl_exec($ch);

curl_setopt($ch, CURLOPT_URL, "https://social.wwu.edu/index.php/statistics/runSloggers/tumblr/0/5");
curl_setopt($ch, CURLOPT_HEADER, 0);
// grab URL and pass it to the browser
curl_exec($ch);

// close cURL resource, and free up system resources
curl_close($ch);

// Uncomment to test emailing of errors...
//echo $somevarthatdoesnotexist;